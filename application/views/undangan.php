<!DOCTYPE html>
<html lang="id-ID" itemscope itemtype="https://schema.org/BlogPosting">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
	<meta charset="UTF-8">

	<style type="text/css">

		.wdp-comment-text img {

			max-width: 100% !important;

		}

	</style>

	<meta name='robots' content='index, follow, max-image-preview:large, max-snippet:-1, max-video-preview:-1' />

	<title>Tiga Bulan Aska - Tridatu Dokumentasi</title>
	<link rel="canonical" href="<?=base_url()?>" />
	<meta property="og:locale" content="id_ID" />
	<meta property="og:type" content="article" />
	<meta property="og:title" content="Tiga Bulan Aska - Tridatu Dokumentasi" />
	<meta property="og:description" content="Kamis, 27 oktober 2022" />
	<meta property="og:url" content="<?=base_url()?>" />
	<meta property="og:site_name" content="Tridatu Dokumentasi" />
	<meta property="article:published_time" content="2022-01-31T06:45:30+00:00" />
	<meta property="article:modified_time" content="2022-01-31T11:26:46+00:00" />
	<meta property="og:image" content="<?=base_url()?>assets/images/logo.ico" />
	<meta property="og:image:width" content="960" />
	<meta property="og:image:height" content="1280" />
	<meta property="og:image:type" content="image/jpeg" />
	<meta name="author" content="Tridatu Dokumentasi" />
	<meta name="twitter:card" content="summary_large_image" />
	<meta name="twitter:label1" content="Ditulis oleh" />
	<meta name="twitter:data1" content="Tridatu Dokumentasi" />
	<meta name="twitter:label2" content="Estimasi waktu membaca" />
	<meta name="twitter:data2" content="4 menit" /><!-- / Yoast SEO plugin. -->

	<style>
		img.wp-smiley,
		img.emoji {
			display: inline !important;
			border: none !important;
			box-shadow: none !important;
			height: 1em !important;
			width: 1em !important;
			margin: 0 0.07em !important;
			vertical-align: -0.1em !important;
			background: none !important;
			padding: 0 !important;
		}
	</style>
	<link rel='stylesheet' id='wp-block-library-css'  href='<?=base_url()?>assets/wp-includes/css/dist/block-library/style.mind9cd.css?ver=5.9.5' media='all' />
	<style id='global-styles-inline-css'>
		body{--wp--preset--color--black: #000000;--wp--preset--color--cyan-bluish-gray: #abb8c3;--wp--preset--color--white: #ffffff;--wp--preset--color--pale-pink: #f78da7;--wp--preset--color--vivid-red: #cf2e2e;--wp--preset--color--luminous-vivid-orange: #ff6900;--wp--preset--color--luminous-vivid-amber: #fcb900;--wp--preset--color--light-green-cyan: #7bdcb5;--wp--preset--color--vivid-green-cyan: #00d084;--wp--preset--color--pale-cyan-blue: #8ed1fc;--wp--preset--color--vivid-cyan-blue: #0693e3;--wp--preset--color--vivid-purple: #9b51e0;--wp--preset--gradient--vivid-cyan-blue-to-vivid-purple: linear-gradient(135deg,rgba(6,147,227,1) 0%,rgb(155,81,224) 100%);--wp--preset--gradient--light-green-cyan-to-vivid-green-cyan: linear-gradient(135deg,rgb(122,220,180) 0%,rgb(0,208,130) 100%);--wp--preset--gradient--luminous-vivid-amber-to-luminous-vivid-orange: linear-gradient(135deg,rgba(252,185,0,1) 0%,rgba(255,105,0,1) 100%);--wp--preset--gradient--luminous-vivid-orange-to-vivid-red: linear-gradient(135deg,rgba(255,105,0,1) 0%,rgb(207,46,46) 100%);--wp--preset--gradient--very-light-gray-to-cyan-bluish-gray: linear-gradient(135deg,rgb(238,238,238) 0%,rgb(169,184,195) 100%);--wp--preset--gradient--cool-to-warm-spectrum: linear-gradient(135deg,rgb(74,234,220) 0%,rgb(151,120,209) 20%,rgb(207,42,186) 40%,rgb(238,44,130) 60%,rgb(251,105,98) 80%,rgb(254,248,76) 100%);--wp--preset--gradient--blush-light-purple: linear-gradient(135deg,rgb(255,206,236) 0%,rgb(152,150,240) 100%);--wp--preset--gradient--blush-bordeaux: linear-gradient(135deg,rgb(254,205,165) 0%,rgb(254,45,45) 50%,rgb(107,0,62) 100%);--wp--preset--gradient--luminous-dusk: linear-gradient(135deg,rgb(255,203,112) 0%,rgb(199,81,192) 50%,rgb(65,88,208) 100%);--wp--preset--gradient--pale-ocean: linear-gradient(135deg,rgb(255,245,203) 0%,rgb(182,227,212) 50%,rgb(51,167,181) 100%);--wp--preset--gradient--electric-grass: linear-gradient(135deg,rgb(202,248,128) 0%,rgb(113,206,126) 100%);--wp--preset--gradient--midnight: linear-gradient(135deg,rgb(2,3,129) 0%,rgb(40,116,252) 100%);--wp--preset--duotone--dark-grayscale: url('#wp-duotone-dark-grayscale');--wp--preset--duotone--grayscale: url('#wp-duotone-grayscale');--wp--preset--duotone--purple-yellow: url('#wp-duotone-purple-yellow');--wp--preset--duotone--blue-red: url('#wp-duotone-blue-red');--wp--preset--duotone--midnight: url('#wp-duotone-midnight');--wp--preset--duotone--magenta-yellow: url('#wp-duotone-magenta-yellow');--wp--preset--duotone--purple-green: url('#wp-duotone-purple-green');--wp--preset--duotone--blue-orange: url('#wp-duotone-blue-orange');--wp--preset--font-size--small: 13px;--wp--preset--font-size--medium: 20px;--wp--preset--font-size--large: 36px;--wp--preset--font-size--x-large: 42px;}.has-black-color{color: var(--wp--preset--color--black) !important;}.has-cyan-bluish-gray-color{color: var(--wp--preset--color--cyan-bluish-gray) !important;}.has-white-color{color: var(--wp--preset--color--white) !important;}.has-pale-pink-color{color: var(--wp--preset--color--pale-pink) !important;}.has-vivid-red-color{color: var(--wp--preset--color--vivid-red) !important;}.has-luminous-vivid-orange-color{color: var(--wp--preset--color--luminous-vivid-orange) !important;}.has-luminous-vivid-amber-color{color: var(--wp--preset--color--luminous-vivid-amber) !important;}.has-light-green-cyan-color{color: var(--wp--preset--color--light-green-cyan) !important;}.has-vivid-green-cyan-color{color: var(--wp--preset--color--vivid-green-cyan) !important;}.has-pale-cyan-blue-color{color: var(--wp--preset--color--pale-cyan-blue) !important;}.has-vivid-cyan-blue-color{color: var(--wp--preset--color--vivid-cyan-blue) !important;}.has-vivid-purple-color{color: var(--wp--preset--color--vivid-purple) !important;}.has-black-background-color{background-color: var(--wp--preset--color--black) !important;}.has-cyan-bluish-gray-background-color{background-color: var(--wp--preset--color--cyan-bluish-gray) !important;}.has-white-background-color{background-color: var(--wp--preset--color--white) !important;}.has-pale-pink-background-color{background-color: var(--wp--preset--color--pale-pink) !important;}.has-vivid-red-background-color{background-color: var(--wp--preset--color--vivid-red) !important;}.has-luminous-vivid-orange-background-color{background-color: var(--wp--preset--color--luminous-vivid-orange) !important;}.has-luminous-vivid-amber-background-color{background-color: var(--wp--preset--color--luminous-vivid-amber) !important;}.has-light-green-cyan-background-color{background-color: var(--wp--preset--color--light-green-cyan) !important;}.has-vivid-green-cyan-background-color{background-color: var(--wp--preset--color--vivid-green-cyan) !important;}.has-pale-cyan-blue-background-color{background-color: var(--wp--preset--color--pale-cyan-blue) !important;}.has-vivid-cyan-blue-background-color{background-color: var(--wp--preset--color--vivid-cyan-blue) !important;}.has-vivid-purple-background-color{background-color: var(--wp--preset--color--vivid-purple) !important;}.has-black-border-color{border-color: var(--wp--preset--color--black) !important;}.has-cyan-bluish-gray-border-color{border-color: var(--wp--preset--color--cyan-bluish-gray) !important;}.has-white-border-color{border-color: var(--wp--preset--color--white) !important;}.has-pale-pink-border-color{border-color: var(--wp--preset--color--pale-pink) !important;}.has-vivid-red-border-color{border-color: var(--wp--preset--color--vivid-red) !important;}.has-luminous-vivid-orange-border-color{border-color: var(--wp--preset--color--luminous-vivid-orange) !important;}.has-luminous-vivid-amber-border-color{border-color: var(--wp--preset--color--luminous-vivid-amber) !important;}.has-light-green-cyan-border-color{border-color: var(--wp--preset--color--light-green-cyan) !important;}.has-vivid-green-cyan-border-color{border-color: var(--wp--preset--color--vivid-green-cyan) !important;}.has-pale-cyan-blue-border-color{border-color: var(--wp--preset--color--pale-cyan-blue) !important;}.has-vivid-cyan-blue-border-color{border-color: var(--wp--preset--color--vivid-cyan-blue) !important;}.has-vivid-purple-border-color{border-color: var(--wp--preset--color--vivid-purple) !important;}.has-vivid-cyan-blue-to-vivid-purple-gradient-background{background: var(--wp--preset--gradient--vivid-cyan-blue-to-vivid-purple) !important;}.has-light-green-cyan-to-vivid-green-cyan-gradient-background{background: var(--wp--preset--gradient--light-green-cyan-to-vivid-green-cyan) !important;}.has-luminous-vivid-amber-to-luminous-vivid-orange-gradient-background{background: var(--wp--preset--gradient--luminous-vivid-amber-to-luminous-vivid-orange) !important;}.has-luminous-vivid-orange-to-vivid-red-gradient-background{background: var(--wp--preset--gradient--luminous-vivid-orange-to-vivid-red) !important;}.has-very-light-gray-to-cyan-bluish-gray-gradient-background{background: var(--wp--preset--gradient--very-light-gray-to-cyan-bluish-gray) !important;}.has-cool-to-warm-spectrum-gradient-background{background: var(--wp--preset--gradient--cool-to-warm-spectrum) !important;}.has-blush-light-purple-gradient-background{background: var(--wp--preset--gradient--blush-light-purple) !important;}.has-blush-bordeaux-gradient-background{background: var(--wp--preset--gradient--blush-bordeaux) !important;}.has-luminous-dusk-gradient-background{background: var(--wp--preset--gradient--luminous-dusk) !important;}.has-pale-ocean-gradient-background{background: var(--wp--preset--gradient--pale-ocean) !important;}.has-electric-grass-gradient-background{background: var(--wp--preset--gradient--electric-grass) !important;}.has-midnight-gradient-background{background: var(--wp--preset--gradient--midnight) !important;}.has-small-font-size{font-size: var(--wp--preset--font-size--small) !important;}.has-medium-font-size{font-size: var(--wp--preset--font-size--medium) !important;}.has-large-font-size{font-size: var(--wp--preset--font-size--large) !important;}.has-x-large-font-size{font-size: var(--wp--preset--font-size--x-large) !important;}
	</style>
	<link rel='stylesheet' id='wpfc-css'  href='<?=base_url()?>assets/wp-content/plugins/gs-facebook-comments/public/css/wpfc-publicba3a.css?ver=1.7.2' media='all' />
	<link rel='stylesheet' id='pafe-extension-style-css'  href='<?=base_url()?>assets/wp-content/plugins/piotnet-addons-for-elementor-pro/assets/css/minify/extension.min856d.css?ver=7.0.6' media='all' />
	<link rel='stylesheet' id='wdp_style-css'  href='<?=base_url()?>assets/wp-content/plugins/weddingpress/addons/comment-kit/css/wdp_styled537.css?ver=2.7.6' media='screen' />
	<style id='wdp_style-inline-css'>


		.wdp-wrapper {

			font-size: 14px

		}



		.wdp-wrapper {

			background: #ffffff;

		}

		.wdp-wrapper.wdp-border {

			border: 1px solid #ffffff;

		}



		.wdp-wrapper .wdp-wrap-comments a:link,

		.wdp-wrapper .wdp-wrap-comments a:visited {

			color: #c9ac74;

		}



		.wdp-wrapper .wdp-wrap-link a.wdp-link {

			color: #c9ac74;

		}

		.wdp-wrapper .wdp-wrap-link a.wdp-link.wdp-icon-link-true .wdpo-comment {

			color: #c9ac74;

		}

		.wdp-wrapper .wdp-wrap-link a.wdp-link:hover {

			color: #2a5782;

		}

		.wdp-wrapper .wdp-wrap-link a.wdp-link:hover .wdpo-comment {

			color: #2a5782;

		}



		.wdp-wrapper .wdp-wrap-form {

			border-top: 1px solid #ffffff;

		}

		.wdp-wrapper .wdp-wrap-form .wdp-container-form textarea.wdp-textarea {

			border: 1px solid #ffffff;

			background: #e5e5e5;

			color: #2b2b2b;

		}

		.wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-info .wdp-post-author {

			background: ;

		}

		.wdp-wrapper .wdp-wrap-form .wdp-container-form input[type='text'] {

			border: 1px solid #ffffff;

			background: #e5e5e5;

			color: #2b2b2b;

		}

		.wdp-wrapper .wdp-wrap-form .wdp-container-form input.wdp-input:focus,

		.wdp-wrapper .wdp-wrap-form .wdp-container-form textarea.wdp-textarea:focus {

			border-color: #64B6EC;

		}

		.wdp-wrapper .wdp-wrap-form .wdp-container-form input[type='submit'],

		.wdp-wrapper .wdp-wrap-form .wdp-container-form input[type='button'].wdp-form-btn {

			color: #FFFFFF;

			background: #c9ac74;

		}

		.wdp-wrapper .wdp-wrap-form .wdp-container-form input[type='submit']:hover,

		.wdp-wrapper .wdp-wrap-form .wdp-container-form input[type='button'].wdp-form-btn:hover {

			background: #c9ac74;

		}

		.wdp-wrapper .wdp-wrap-form .wdp-container-form .wdp-captcha .wdp-captcha-text {

			color: #2b2b2b;

		}



		.wdp-wrapper .wdp-media-btns a > span {

			color: #c9ac74;

		}

		.wdp-wrapper .wdp-media-btns a > span:hover {

			color: #2a5782;

		}



		.wdp-wrapper .wdp-comment-status {

			border-top: 1px solid #ffffff;

		}

		.wdp-wrapper .wdp-comment-status p.wdp-ajax-success {

			color: #319342;

		}

		.wdp-wrapper .wdp-comment-status p.wdp-ajax-error {

			color: #C85951;

		}

		.wdp-wrapper .wdp-comment-status.wdp-loading > span {

			color: #c9ac74;

		}



		.wdp-wrapper ul.wdp-container-comments {

			border-top: 1px solid #ffffff;

		}

		.wdp-wrapper ul.wdp-container-comments li.wdp-item-comment {

			border-bottom: 1px solid #ffffff;

		}

		.wdp-wrapper ul.wdp-container-comments li.wdp-item-comment ul li.wdp-item-comment {

			border-top: 1px solid #ffffff;

		}

		.wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-info a.wdp-commenter-name {

			color: #c9ac74 !important;

		}

		.wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-info a.wdp-commenter-name:hover {

			color: #2a5782 !important;

		}

		.wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-info .wdp-comment-time {

			color: #9DA8B7;

		}

		.wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-text p {

			color: #2b2b2b;

		}

		.wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-actions a {

			color: #c9ac74;

		}

		.wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-actions a:hover {

			color: #2a5782;

		}

		.wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-rating .wdp-rating-link > span {

			color: #c9cfd7;

		}

		.wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-rating .wdp-rating-link > span:hover {

			color: #3D7DBC;

		}

		.wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-rating .wdp-rating-count {

			color: #9DA8B7;

		}

		.wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-rating .wdp-rating-count.wdp-rating-positive {

			color: #2C9E48;

		}

		.wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-rating .wdp-rating-count.wdp-rating-negative {

			color: #D13D3D;

		}

		.wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-rating .wdp-rating-count.wdpo-loading {

			color: #c9cfd7;

		}

		.wdp-wrapper ul.wdp-container-comments a.wdp-load-more-comments:hover {

			color: #2a5782;

		}



		.wdp-wrapper .wdp-counter-info {

			color: #9DA8B7;

		}



		.wdp-wrapper .wdp-holder span {

			color: #c9ac74;

		}

		.wdp-wrapper .wdp-holder a,

		.wdp-wrapper .wdp-holder a:link,

		.wdp-wrapper .wdp-holder a:visited {

			color: #c9ac74;

		}

		.wdp-wrapper .wdp-holder a:hover,

		.wdp-wrapper .wdp-holder a:link:hover,

		.wdp-wrapper .wdp-holder a:visited:hover {

			color: #2a5782;

		}

		.wdp-wrapper .wdp-holder a.jp-previous.jp-disabled, .wdp-wrapper .wdp-holder a.jp-previous.jp-disabled:hover, .wdp-wrapper .wdp-holder a.jp-next.jp-disabled, .wdp-wrapper .wdp-holder a.jp-next.jp-disabled:hover {

			color: #9DA8B7;

		}



		.wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-avatar img {

			max-width: 28px;

			max-height: 28px;

		}

		.wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content {

			margin-left: 38px;

		}

		.wdp-wrapper ul.wdp-container-comments li.wdp-item-comment ul .wdp-comment-avatar img {

			max-width: 24px;

			max-height: 24px;

		}

		.wdp-wrapper ul.wdp-container-comments li.wdp-item-comment ul ul .wdp-comment-avatar img {

			max-width: 21px;

			max-height: 21px;

		}


	</style>
	<link rel='stylesheet' id='wdp-centered-css-css'  href='<?=base_url()?>assets/wp-content/plugins/weddingpress/assets/css/wdp-centered-timeline.mind9cd.css?ver=5.9.5' media='all' />
	<link rel='stylesheet' id='wdp-horizontal-css-css'  href='<?=base_url()?>assets/wp-content/plugins/weddingpress/assets/css/wdp-horizontal-styles.mind9cd.css?ver=5.9.5' media='all' />
	<link rel='stylesheet' id='wdp-fontello-css-css'  href='<?=base_url()?>assets/wp-content/plugins/weddingpress/assets/css/wdp-fontellod9cd.css?ver=5.9.5' media='all' />
	<link rel='stylesheet' id='exad-main-style-css'  href='<?=base_url()?>assets/wp-content/plugins/weddingpress/assets/css/exad-styles.mind9cd.css?ver=5.9.5' media='all' />
	<link rel='stylesheet' id='hello-elementor-css'  href='<?=base_url()?>assets/wp-content/themes/hello-elementor/style.minc141.css?ver=2.6.1' media='all' />
	<link rel='stylesheet' id='hello-elementor-theme-style-css'  href='<?=base_url()?>assets/wp-content/themes/hello-elementor/theme.minc141.css?ver=2.6.1' media='all' />
	<link rel='stylesheet' id='elementor-frontend-legacy-css'  href='<?=base_url()?>assets/wp-content/plugins/elementor/assets/css/frontend-legacy.mina4f3.css?ver=3.6.8' media='all' />
	<link rel='stylesheet' id='elementor-frontend-css'  href='<?=base_url()?>assets/wp-content/plugins/elementor/assets/css/frontend.mina4f3.css?ver=3.6.8' media='all' />
	<link rel='stylesheet' id='eael-general-css'  href='<?=base_url()?>assets/wp-content/plugins/essential-addons-for-elementor-lite/assets/front-end/css/view/general.min9dff.css?ver=5.3.2' media='all' />
	<link rel='stylesheet' id='eael-34320-css'  href='<?=base_url()?>assets/wp-content/uploads/essential-addons-elementor/eael-343204455.css?ver=1643628406' media='all' />
	<link rel='stylesheet' id='elementor-icons-css'  href='<?=base_url()?>assets/wp-content/plugins/elementor/assets/lib/eicons/css/elementor-icons.min7816.css?ver=5.15.0' media='all' />
	<style id='elementor-icons-inline-css'>

		.elementor-add-new-section .elementor-add-templately-promo-button{
			background-color: #5d4fff;
			background-image: url(https://undangonlinebali.com/wp-content/plugins/essential-addons-for-elementor-lite/assets/admin/images/templately/logo-icon.svg);
			background-repeat: no-repeat;
			background-position: center center;
			margin-left: 5px;
			position: relative;
			bottom: 5px;
		}

		.elementor-add-new-section .elementor-add-templately-promo-button{
			background-color: #5d4fff;
			background-image: url(https://undangonlinebali.com/wp-content/plugins/essential-addons-for-elementor-lite/assets/admin/images/templately/logo-icon.svg);
			background-repeat: no-repeat;
			background-position: center center;
			margin-left: 5px;
			position: relative;
			bottom: 5px;
		}
	</style>
	<link rel='stylesheet' id='elementor-post-10-css'  href='<?=base_url()?>assets/wp-content/uploads/elementor/css/post-100400.css?ver=1652750519' media='all' />
	<link rel='stylesheet' id='powerpack-frontend-css'  href='<?=base_url()?>assets/wp-content/plugins/powerpack-elements/assets/css/min/frontend.min562c.css?ver=2.9.11' media='all' />
	<link rel='stylesheet' id='weddingpress-wdp-css'  href='<?=base_url()?>assets/wp-content/plugins/weddingpress/assets/css/wdp292d.css?ver=2.8.8' media='all' />
	<link rel='stylesheet' id='elementor-pro-css'  href='<?=base_url()?>assets/wp-content/plugins/elementor-pro/assets/css/frontend.min1ac1.css?ver=3.7.3' media='all' />
	<link rel='stylesheet' id='elementor-global-css'  href='<?=base_url()?>assets/wp-content/uploads/elementor/css/globalebec.css?ver=1652750525' media='all' />
	<link rel='stylesheet' id='elementor-post-34320-css'  href='<?=base_url()?>assets/wp-content/uploads/elementor/css/post-34320a580.css?ver=1652977533' media='all' />
	<link rel='stylesheet' id='elementor-post-29971-css'  href='<?=base_url()?>assets/wp-content/uploads/elementor/css/post-29971a580.css?ver=1652977533' media='all' />
	<link rel='stylesheet' id='elementor-post-44314-css'  href='<?=base_url()?>assets/wp-content/uploads/elementor/css/post-4431406cc.css?ver=1657207305' media='all' />
	<link rel='stylesheet' id='elementor-post-32749-css'  href='<?=base_url()?>assets/wp-content/uploads/elementor/css/post-327499035.css?ver=1652750520' media='all' />
	<link rel='stylesheet' id='elementor-icons-ekiticons-css'  href='<?=base_url()?>assets/wp-content/plugins/elementskit-lite/modules/elementskit-icon-pack/assets/css/ekiticonsde39.css?ver=2.7.3' media='all' />
	<link rel='stylesheet' id='ekit-widget-styles-css'  href='<?=base_url()?>assets/wp-content/plugins/elementskit-lite/widgets/init/assets/css/widget-stylesde39.css?ver=2.7.3' media='all' />
	<link rel='stylesheet' id='ekit-responsive-css'  href='<?=base_url()?>assets/wp-content/plugins/elementskit-lite/widgets/init/assets/css/responsivede39.css?ver=2.7.3' media='all' />
	<link rel='stylesheet' id='google-fonts-1-css'  href='https://fonts.googleapis.com/css?family=Roboto%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CRoboto+Slab%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CPT+Sans%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CDavid+Libre%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CPlayfair+Display%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CPoppins%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CSpartan%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CABeeZee%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CCourgette%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CDidact+Gothic%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CGreat+Vibes%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CAlata%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic&#038;display=auto&#038;ver=5.9.5' media='all' />
	<link rel='stylesheet' id='elementor-icons-shared-0-css'  href='<?=base_url()?>assets/wp-content/plugins/elementor/assets/lib/font-awesome/css/fontawesome.min52d5.css?ver=5.15.3' media='all' />
	<link rel='stylesheet' id='elementor-icons-fa-regular-css'  href='<?=base_url()?>assets/wp-content/plugins/elementor/assets/lib/font-awesome/css/regular.min52d5.css?ver=5.15.3' media='all' />
	<link rel='stylesheet' id='elementor-icons-fa-solid-css'  href='<?=base_url()?>assets/wp-content/plugins/elementor/assets/lib/font-awesome/css/solid.min52d5.css?ver=5.15.3' media='all' />
	<link rel='stylesheet' id='elementor-icons-fa-brands-css'  href='<?=base_url()?>assets/wp-content/plugins/elementor/assets/lib/font-awesome/css/brands.min52d5.css?ver=5.15.3' media='all' />
	<script id='jquery-core-js-extra'>
		var pp = {"ajax_url":"https:\/\/undangonlinebali.com\/wp-admin\/admin-ajax.php"};
	</script>
	<script src='<?=base_url()?>assets/wp-includes/js/jquery/jquery.minaf6c.js?ver=3.6.0' id='jquery-core-js'></script>
	<script src='<?=base_url()?>assets/wp-includes/js/jquery/jquery-migrate.mind617.js?ver=3.3.2' id='jquery-migrate-js'></script>
	<script src='<?=base_url()?>assets/wp-content/plugins/gs-facebook-comments/public/js/wpfc-publicba3a.js?ver=1.7.2' id='wpfc-js'></script>
	<script src='<?=base_url()?>assets/wp-content/plugins/piotnet-addons-for-elementor-pro/assets/js/minify/extension.min856d.js?ver=7.0.6' id='pafe-extension-js'></script>
	<meta property="profile:username" content="Tridatu Dokumentasi" />
	<!-- /OG -->

	<link rel="https://api.w.org/" href="https://undangonlinebali.com/wp-json/" /><link rel="alternate" type="application/json" href="https://undangonlinebali.com/wp-json/wp/v2/posts/34320" /><link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://undangonlinebali.com/xmlrpc.php?rsd" />
	<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://undangonlinebali.com/wp-includes/wlwmanifest.xml" />
	<meta name="generator" content="WordPress 5.9.5" />
	<link rel='shortlink' href='https://undangonlinebali.com/?p=34320' />
	<meta property="fb:app_id" content="340582007667482" /><link href="<?=base_url()?>assets/unpkg.com/aos%402.3.1/dist/aos.css" rel="stylesheet">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<link rel="icon" href="<?=base_url()?>assets/images/logo.ico" sizes="32x32" />
	<link rel="icon" href="<?=base_url()?>assets/images/logo.ico" sizes="192x192" />
	<link rel="apple-touch-icon" href="<?=base_url()?>assets/images/logo.icog" />
	<meta name="msapplication-TileImage" content="<?=base_url()?>assets/images/logo.icog" />
	<style>.pswp.pafe-lightbox-modal {display: none;}</style>	<meta name="viewport" content="width=device-width, initial-scale=1.0, viewport-fit=cover" /></head>
<body class="post-template post-template-elementor_canvas single single-post postid-34320 single-format-standard elementor-default elementor-template-canvas elementor-kit-10 elementor-page elementor-page-34320">
<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;" ><defs><filter id="wp-duotone-dark-grayscale"><feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " /><feComponentTransfer color-interpolation-filters="sRGB" ><feFuncR type="table" tableValues="0 0.49803921568627" /><feFuncG type="table" tableValues="0 0.49803921568627" /><feFuncB type="table" tableValues="0 0.49803921568627" /><feFuncA type="table" tableValues="1 1" /></feComponentTransfer><feComposite in2="SourceGraphic" operator="in" /></filter></defs></svg><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;" ><defs><filter id="wp-duotone-grayscale"><feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " /><feComponentTransfer color-interpolation-filters="sRGB" ><feFuncR type="table" tableValues="0 1" /><feFuncG type="table" tableValues="0 1" /><feFuncB type="table" tableValues="0 1" /><feFuncA type="table" tableValues="1 1" /></feComponentTransfer><feComposite in2="SourceGraphic" operator="in" /></filter></defs></svg><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;" ><defs><filter id="wp-duotone-purple-yellow"><feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " /><feComponentTransfer color-interpolation-filters="sRGB" ><feFuncR type="table" tableValues="0.54901960784314 0.98823529411765" /><feFuncG type="table" tableValues="0 1" /><feFuncB type="table" tableValues="0.71764705882353 0.25490196078431" /><feFuncA type="table" tableValues="1 1" /></feComponentTransfer><feComposite in2="SourceGraphic" operator="in" /></filter></defs></svg><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;" ><defs><filter id="wp-duotone-blue-red"><feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " /><feComponentTransfer color-interpolation-filters="sRGB" ><feFuncR type="table" tableValues="0 1" /><feFuncG type="table" tableValues="0 0.27843137254902" /><feFuncB type="table" tableValues="0.5921568627451 0.27843137254902" /><feFuncA type="table" tableValues="1 1" /></feComponentTransfer><feComposite in2="SourceGraphic" operator="in" /></filter></defs></svg><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;" ><defs><filter id="wp-duotone-midnight"><feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " /><feComponentTransfer color-interpolation-filters="sRGB" ><feFuncR type="table" tableValues="0 0" /><feFuncG type="table" tableValues="0 0.64705882352941" /><feFuncB type="table" tableValues="0 1" /><feFuncA type="table" tableValues="1 1" /></feComponentTransfer><feComposite in2="SourceGraphic" operator="in" /></filter></defs></svg><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;" ><defs><filter id="wp-duotone-magenta-yellow"><feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " /><feComponentTransfer color-interpolation-filters="sRGB" ><feFuncR type="table" tableValues="0.78039215686275 1" /><feFuncG type="table" tableValues="0 0.94901960784314" /><feFuncB type="table" tableValues="0.35294117647059 0.47058823529412" /><feFuncA type="table" tableValues="1 1" /></feComponentTransfer><feComposite in2="SourceGraphic" operator="in" /></filter></defs></svg><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;" ><defs><filter id="wp-duotone-purple-green"><feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " /><feComponentTransfer color-interpolation-filters="sRGB" ><feFuncR type="table" tableValues="0.65098039215686 0.40392156862745" /><feFuncG type="table" tableValues="0 1" /><feFuncB type="table" tableValues="0.44705882352941 0.4" /><feFuncA type="table" tableValues="1 1" /></feComponentTransfer><feComposite in2="SourceGraphic" operator="in" /></filter></defs></svg><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;" ><defs><filter id="wp-duotone-blue-orange"><feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " /><feComponentTransfer color-interpolation-filters="sRGB" ><feFuncR type="table" tableValues="0.098039215686275 1" /><feFuncG type="table" tableValues="0 0.66274509803922" /><feFuncB type="table" tableValues="0.84705882352941 0.41960784313725" /><feFuncA type="table" tableValues="1 1" /></feComponentTransfer><feComposite in2="SourceGraphic" operator="in" /></filter></defs></svg>		<div data-elementor-type="wp-post" data-elementor-id="34320" class="elementor elementor-34320">
	<div class="elementor-inner">
		<div class="elementor-section-wrap">
			<section class="elementor-section elementor-top-section elementor-element elementor-element-233297b7 elementor-section-height-min-height elementor-section-items-bottom elementor-section-content-middle elementor-section-full_width elementor-section-height-default wdp-sticky-section-no" data-id="233297b7" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}" style="background-image: url('<?=base_url()?>assets/images/cover.jpg') !important;">
				<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-row">
						<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-c627be0 wdp-sticky-section-no elementor-invisible" data-id="c627be0" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;animation&quot;:&quot;zoomInUp&quot;}">
							<div class="elementor-column-wrap elementor-element-populated">
								<div class="elementor-background-overlay"></div>
								<div class="elementor-widget-wrap">
									<div class="elementor-element elementor-element-3d3a93e6 wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="3d3a93e6" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
										<div class="elementor-widget-container">
											<p class="elementor-heading-title elementor-size-default" style="color: #d99a14">Upacara<br>Tiga Bulan</p>		</div>
									</div>
									<div class="elementor-element elementor-element-593686b1 wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="593686b1" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;none&quot;}" data-aos="fade-up" data-widget_type="heading.default">
										<div class="elementor-widget-container">
											<p class="elementor-heading-title elementor-size-default" style="color: #d99a14">Aska</p>		</div>
									</div>
									<div class="elementor-element elementor-element-2bd6a6d6 wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="2bd6a6d6" data-element_type="widget" data-aos="zoom-in" data-aos-duration="1000" data-widget_type="image.default">
										<div class="elementor-widget-container">
											<div class="elementor-image">
												<img width="800" height="131" src="<?=base_url()?>assets/wp-content/uploads/2021/09/5d-1024x168.png" class="attachment-large size-large" alt="" loading="lazy" sizes="(max-width: 800px) 100vw, 800px" />														</div>
										</div>
									</div>
									<div class="elementor-element elementor-element-70817ca3 wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="70817ca3" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;none&quot;}" data-aos="zoom-in" data-widget_type="heading.default">
										<div class="elementor-widget-container">
											<p class="elementor-heading-title elementor-size-default" style="color: #d99a14">27 oktober 2022</p>		</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section class="elementor-section elementor-top-section elementor-element elementor-element-73abc121 menu_sticky elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="73abc121" data-element_type="section">
				<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-row">
						<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-60681b84 wdp-sticky-section-no" data-id="60681b84" data-element_type="column">
							<div class="elementor-column-wrap elementor-element-populated">
								<div class="elementor-widget-wrap">
									<div class="elementor-element elementor-element-31464566 wdp-sticky-section-no elementor-widget elementor-widget-html" data-id="31464566" data-element_type="widget" data-widget_type="html.default">
										<div class="elementor-widget-container">
											<style>
												/*Menu*/
												.menu_sticky {
													position: fixed;
													bottom: 0;
													left: 0;
													width: 100%
												}
											</style>
										</div>
									</div>
									<section class="elementor-section elementor-inner-section elementor-element elementor-element-7f6e9b26 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="7f6e9b26" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
										<div class="elementor-container elementor-column-gap-default">
											<div class="elementor-row">
												<div class="elementor-column elementor-col-10" style="width: 10%"></div>
												<div class="elementor-column elementor-col-20 elementor-inner-column elementor-element elementor-element-2d413c14 wdp-sticky-section-no" data-id="2d413c14" data-element_type="column">
													<div class="elementor-column-wrap elementor-element-populated">
														<div class="elementor-widget-wrap">
															<div class="elementor-element elementor-element-6c8fdec elementor-view-stacked elementor-shape-circle wdp-sticky-section-no elementor-widget elementor-widget-icon" data-id="6c8fdec" data-element_type="widget" data-widget_type="icon.default">
																<div class="elementor-widget-container">
																	<div class="elementor-icon-wrapper">
																		<a class="elementor-icon" href="#mempelai">
																			<i aria-hidden="true" class="icon icon-rings"></i>			</a>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="elementor-column elementor-col-20 elementor-inner-column elementor-element elementor-element-7f5c49ba wdp-sticky-section-no" data-id="7f5c49ba" data-element_type="column">
													<div class="elementor-column-wrap elementor-element-populated">
														<div class="elementor-widget-wrap">
															<div class="elementor-element elementor-element-2d7b64d0 elementor-view-stacked elementor-shape-circle wdp-sticky-section-no elementor-widget elementor-widget-icon" data-id="2d7b64d0" data-element_type="widget" data-widget_type="icon.default">
																<div class="elementor-widget-container">
																	<div class="elementor-icon-wrapper">
																		<a class="elementor-icon" href="#date">
																			<i aria-hidden="true" class="far fa-calendar-alt"></i>			</a>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="elementor-column elementor-col-20 elementor-inner-column elementor-element elementor-element-28f40fb4 wdp-sticky-section-no" data-id="28f40fb4" data-element_type="column">
													<div class="elementor-column-wrap elementor-element-populated">
														<div class="elementor-widget-wrap">
															<div class="elementor-element elementor-element-48d1d60f elementor-view-stacked elementor-shape-circle wdp-sticky-section-no elementor-widget elementor-widget-icon" data-id="48d1d60f" data-element_type="widget" data-widget_type="icon.default">
																<div class="elementor-widget-container">
																	<div class="elementor-icon-wrapper">
																		<a class="elementor-icon" href="#map">
																			<i aria-hidden="true" class="fas fa-map-marker-alt"></i>			</a>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="elementor-column elementor-col-20 elementor-inner-column elementor-element elementor-element-1969ef73 wdp-sticky-section-no" data-id="1969ef73" data-element_type="column">
													<div class="elementor-column-wrap elementor-element-populated">
														<div class="elementor-widget-wrap">
															<div class="elementor-element elementor-element-6ce89f20 elementor-view-stacked elementor-shape-circle wdp-sticky-section-no elementor-widget elementor-widget-weddingpress-audio" data-id="6ce89f20" data-element_type="widget" data-widget_type="weddingpress-audio.default">
																<div class="elementor-widget-container">

																	<script>
																		var settingAutoplay = 'yes';
																		window.settingAutoplay = settingAutoplay === 'disable' ? false : true;
																	</script>

																	<div id="audio-container" class="audio-box">

																		<audio id="song" loop>
																			<source src="<?=base_url()?>assets/audio/backsound.mp3"
																					type="audio/mp3">
																		</audio>

																		<div class="elementor-icon-wrapper" id="unmute-sound" style="display: none;">
																			<div class="elementor-icon">
																				<i aria-hidden="true" class="fas fa-music"></i>				</div>
																		</div>

																		<div class="elementor-icon-wrapper" id="mute-sound" style="display: none;">
																			<div class="elementor-icon">
																				<i aria-hidden="true" class="fas fa-volume-mute"></i>				</div>
																		</div>

																	</div>

																</div>
															</div>
														</div>
													</div>
												</div>

												<div class="elementor-column elementor-col-10 elementor-inner-column elementor-element elementor-element-2d413c14 wdp-sticky-section-no" data-id="2d413c14" data-element_type="column"></div>
											</div>
										</div>
									</section>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section class="elementor-section elementor-top-section elementor-element elementor-element-1391b6bc elementor-section-full_width elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="1391b6bc" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;gradient&quot;,&quot;shape_divider_bottom&quot;:&quot;opacity-tilt&quot;}">
				<div class="elementor-shape elementor-shape-bottom" data-negative="false">
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 2600 131.1" preserveAspectRatio="none">
						<path class="elementor-shape-fill" d="M0 0L2600 0 2600 69.1 0 0z"/>
						<path class="elementor-shape-fill" style="opacity:0.5" d="M0 0L2600 0 2600 69.1 0 69.1z"/>
						<path class="elementor-shape-fill" style="opacity:0.25" d="M2600 0L0 0 0 130.1 2600 69.1z"/>
					</svg>		</div>
				<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-row">
						<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-3dc7cc0f wdp-sticky-section-no" data-id="3dc7cc0f" data-element_type="column">
							<div class="elementor-column-wrap elementor-element-populated">
								<div class="elementor-widget-wrap">
									<div class="elementor-element elementor-element-4ddf0168 wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="4ddf0168" data-element_type="widget" data-widget_type="image.default">
										<div class="elementor-widget-container">
											<div class="elementor-image">
												<img width="800" height="416" src="<?=base_url()?>assets/wp-content/uploads/2021/05/14398-Converted-1024x533.png" class="attachment-large size-large" alt="" loading="lazy" sizes="(max-width: 800px) 100vw, 800px" />														</div>
										</div>
									</div>
									<div class="elementor-element elementor-element-6f5f3b0e wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="6f5f3b0e" data-element_type="widget" data-aos="zoom-in" data-widget_type="image.default">
										<div class="elementor-widget-container">
											<div class="elementor-image">
												<img width="1088" height="179" src="<?=base_url()?>assets/wp-content/uploads/2021/12/4d.png" class="attachment-full size-full" alt="" loading="lazy" sizes="(max-width: 1088px) 100vw, 1088px" />														</div>
										</div>
									</div>
									<div class="elementor-element elementor-element-11fae19a wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="11fae19a" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;none&quot;}" data-aos="zoom-in" data-aos-duration="1000" data-widget_type="heading.default">
										<div class="elementor-widget-container">
											<p class="elementor-heading-title elementor-size-default">Om Swastyastu
											</p>		</div>
									</div>
									<div class="elementor-element elementor-element-8986702 wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="8986702" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;none&quot;}" data-aos="zoom-in" data-aos-duration="1000" data-widget_type="heading.default">
										<div class="elementor-widget-container">
											<p class="elementor-heading-title elementor-size-default">Atas Asung Kertha Wara Nugraha<br> Ida Sang Hyang Widhi Wasa/Tuhan Yang Maha Esa, perkenankan kami<br>mengundang Bapak/Ibu/Saudara/i pada Upacara Tiga Bulan putra kami.</p>		</div>
									</div>
									<div class="elementor-element elementor-element-50a706d8 wdp-sticky-section-no elementor-widget elementor-widget-menu-anchor" data-id="50a706d8" data-element_type="widget" data-widget_type="menu-anchor.default">
										<div class="elementor-widget-container">
											<div id="mempelai" class="elementor-menu-anchor"></div>
										</div>
									</div>
									<section class="elementor-section elementor-inner-section elementor-element elementor-element-4db8bb9e elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="4db8bb9e" data-element_type="section">
										<div class="elementor-container elementor-column-gap-default">
											<div class="elementor-row">
												<div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-5fa83d16 wdp-sticky-section-no" data-id="5fa83d16" data-element_type="column">
													<div class="elementor-column-wrap elementor-element-populated">
														<div class="elementor-widget-wrap">
															<div class="elementor-element elementor-element-6ffc3a8a wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="6ffc3a8a" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;none&quot;,&quot;_animation&quot;:&quot;none&quot;}" data-aos="zoom-in" data-aos-duration="1000" data-widget_type="image.default">

															</div>
															<div class="elementor-element elementor-element-3186aa5c wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="39d3da67" data-element_type="widget" data-widget_type="image.default">
																<div class="elementor-widget-container">
																	<p class="elementor-heading-title elementor-size-default" style="font-size: 150px">Aska</p>		</div>
															</div>
															<div class="elementor-element elementor-element-3186aa5c wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="3186aa5c" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;none&quot;,&quot;_animation&quot;:&quot;none&quot;}" data-aos="zoom-in" data-aos-duration="1000" data-widget_type="heading.default">
																<div class="elementor-widget-container">
																	<p class="elementor-heading-title elementor-size-default">I Putu Aska</p>		</div>
															</div>
															<div class="elementor-element elementor-element-75f2e658 wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="75f2e658" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;none&quot;,&quot;_animation&quot;:&quot;none&quot;}" data-aos="zoom-in" data-aos-duration="1000" data-widget_type="heading.default">
																<div class="elementor-widget-container">
																	<p class="elementor-heading-title elementor-size-default"><br><b>Putra dari Pasangan<br><br></b> I Kadek Yudiarta<br>&amp;<br>Ni Luh Komala Dewi
																	</p>		</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</section>
									<div class="elementor-element elementor-element-51c45cef wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="51c45cef" data-element_type="widget" data-widget_type="spacer.default">
										<div class="elementor-widget-container">
											<div class="elementor-spacer">
												<div class="elementor-spacer-inner"></div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section class="elementor-section elementor-top-section elementor-element elementor-element-7c88d46 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="7c88d46" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;gradient&quot;,&quot;shape_divider_bottom&quot;:&quot;opacity-tilt&quot;}">
				<div class="elementor-background-overlay"></div>
				<div class="elementor-shape elementor-shape-bottom" data-negative="false">
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 2600 131.1" preserveAspectRatio="none">
						<path class="elementor-shape-fill" d="M0 0L2600 0 2600 69.1 0 0z"/>
						<path class="elementor-shape-fill" style="opacity:0.5" d="M0 0L2600 0 2600 69.1 0 69.1z"/>
						<path class="elementor-shape-fill" style="opacity:0.25" d="M2600 0L0 0 0 130.1 2600 69.1z"/>
					</svg>		</div>
				<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-row">
						<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-68e184d5 wdp-sticky-section-no" data-id="68e184d5" data-element_type="column">
							<div class="elementor-column-wrap elementor-element-populated">
								<div class="elementor-widget-wrap">
									<div class="elementor-element elementor-element-74590615 wdp-sticky-section-no elementor-widget elementor-widget-menu-anchor" data-id="74590615" data-element_type="widget" data-widget_type="menu-anchor.default">
										<div class="elementor-widget-container">
											<div id="date" class="elementor-menu-anchor"></div>
										</div>
									</div>

									<section class="elementor-section elementor-inner-section elementor-element elementor-element-66e671fa elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="66e671fa" data-element_type="section">
										<div class="elementor-background-overlay"></div>
										<div class="elementor-container elementor-column-gap-default">
											<div class="elementor-row">
												<div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-4d0c9502 wdp-sticky-section-no" data-id="4d0c9502" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;gradient&quot;}">
													<div class="elementor-column-wrap elementor-element-populated">
														<div class="elementor-widget-wrap">
															<div class="elementor-element elementor-element-987fc42 wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="987fc42" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;none&quot;}" data-aos="zoom-in" data-aos-duration="1000" data-widget_type="image.default">
																<div class="elementor-widget-container">
																	<div class="elementor-image">
																		<img width="800" height="131" src="<?=base_url()?>assets/wp-content/uploads/2021/12/4d-1024x168.png" class="attachment-large size-large" alt="" loading="lazy"  sizes="(max-width: 800px) 100vw, 800px" />														</div>
																</div>
															</div>
															<div class="elementor-element elementor-element-62958056 wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="62958056" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;none&quot;,&quot;_animation&quot;:&quot;none&quot;}" data-aos="zoom-in" data-aos-duration="1000" data-widget_type="heading.default">
																<div class="elementor-widget-container">
																	<h2 class="elementor-heading-title elementor-size-default">Acara</h2>		</div>
															</div>
															<div class="elementor-element elementor-element-1ab84dc0 wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="1ab84dc0" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;none&quot;}" data-aos="zoom-in" data-aos-duration="1000" data-widget_type="image.default">
																<div class="elementor-widget-container">
																	<div class="elementor-image">
																		<img width="800" height="131" src="<?=base_url()?>assets/wp-content/uploads/2021/09/5d-1024x168.png" class="attachment-large size-large" alt="" loading="lazy"  sizes="(max-width: 800px) 100vw, 800px" />														</div>
																</div>
															</div>
															<section class="elementor-section elementor-inner-section elementor-element elementor-element-6c86904b elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="6c86904b" data-element_type="section">
																<div class="elementor-container elementor-column-gap-default">
																	<div class="elementor-row">
																		<div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-53beec11 wdp-sticky-section-no" data-id="53beec11" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
																			<div class="elementor-column-wrap elementor-element-populated">
																				<div class="elementor-widget-wrap">
																					<div class="elementor-element elementor-element-30c1a929 elementor-view-framed elementor-shape-circle elementor-mobile-position-top elementor-vertical-align-top wdp-sticky-section-no elementor-widget elementor-widget-icon-box" data-id="30c1a929" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;none&quot;}" data-aos="zoom-in" data-aos-duration="1000" data-widget_type="icon-box.default">
																						<div class="elementor-widget-container">
																							<div class="elementor-icon-box-wrapper">
																								<div class="elementor-icon-box-icon">
				<span class="elementor-icon elementor-animation-" >
				<i aria-hidden="true" class="far fa-calendar-alt"></i>				</span>
																								</div>
																								<div class="elementor-icon-box-content">
																									<h3 class="elementor-icon-box-title">
					<span  >
											</span>
																									</h3>
																									<p class="elementor-icon-box-description">
																										Kamis,<br>27 oktober 2022					</p>
																								</div>
																							</div>
																						</div>
																					</div>
																				</div>
																			</div>
																		</div>
																		<div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-55e10671 wdp-sticky-section-no" data-id="55e10671" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
																			<div class="elementor-column-wrap elementor-element-populated">
																				<div class="elementor-widget-wrap">
																					<div class="elementor-element elementor-element-66da6355 elementor-view-framed elementor-shape-circle elementor-mobile-position-top elementor-vertical-align-top wdp-sticky-section-no elementor-widget elementor-widget-icon-box" data-id="66da6355" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;none&quot;}" data-aos="zoom-in" data-aos-duration="1000" data-widget_type="icon-box.default">
																						<div class="elementor-widget-container">
																							<div class="elementor-icon-box-wrapper">
																								<div class="elementor-icon-box-icon">
				<span class="elementor-icon elementor-animation-" >
				<i aria-hidden="true" class="far fa-clock"></i>				</span>
																								</div>
																								<div class="elementor-icon-box-content">
																									<h3 class="elementor-icon-box-title">
					<span  >
											</span>
																									</h3>
																									<p class="elementor-icon-box-description">
																										Pukul  : <br>10.00 WITA - Selesai					</p>
																								</div>
																							</div>
																						</div>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</section>
															<div class="elementor-element elementor-element-4d011d7b wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="4d011d7b" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;none&quot;,&quot;_animation&quot;:&quot;none&quot;}" data-aos="zoom-in" data-aos-duration="1000" data-widget_type="heading.default">
																<div class="elementor-widget-container">
																	<p class="elementor-heading-title elementor-size-default">Rumah Kediaman :<br><br>
																		Jl. Bidadari II No.2, Kerobokan Kelod, Kec. Kuta Utara, Kabupaten Badung, Bali 80361</p>		</div>
															</div>
															<div class="elementor-element elementor-element-19b0595e wdp-sticky-section-no elementor-widget elementor-widget-menu-anchor" data-id="19b0595e" data-element_type="widget" data-widget_type="menu-anchor.default">
																<div class="elementor-widget-container">
																	<div id="map" class="elementor-menu-anchor"></div>
																</div>
															</div>
															<div class="elementor-element elementor-element-628d97b1 animated-fast wdp-sticky-section-no elementor-widget elementor-widget-google_maps" data-id="628d97b1" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;none&quot;}" data-aos="zoom-in" data-aos-duration="1000" data-widget_type="google_maps.default">
																<div class="elementor-widget-container">
																	<div class="elementor-custom-embed">
																		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3944.1006108863508!2d115.16971840000001!3d-8.681981900000002!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x70c9dfcc36c4d4b!2sBONEKA%20KANGEN!5e0!3m2!1sen!2sid!4v1666342789370!5m2!1sen!2sid" width="400" height="300" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
																	</div>
																</div>
															</div>
															<div class="elementor-element elementor-element-4add0092 elementor-align-center animated-fast wdp-sticky-section-no elementor-widget elementor-widget-button" data-id="4add0092" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;none&quot;}" data-aos="zoom-in" data-aos-duration="1000" data-widget_type="button.default">
																<div class="elementor-widget-container">
																	<div class="elementor-button-wrapper">
																		<a href="https://goo.gl/maps/vsFH4b2sVGkZ9Ww17" class="elementor-button-link elementor-button elementor-size-sm" role="button">
						<span class="elementor-button-content-wrapper">
							<span class="elementor-button-icon elementor-align-icon-left">
				<i aria-hidden="true" class="fas fa-map-marker-alt"></i>			</span>
						<span class="elementor-button-text">Petunjuk Ke Lokasi</span>
		</span>
																		</a>
																	</div>
																</div>
															</div>
															<div class="elementor-element elementor-element-7f3bd3dd wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="7f3bd3dd" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;none&quot;}" data-aos="zoom-in" data-aos-duration="1000" data-widget_type="heading.default">
																<div class="elementor-widget-container">
																	<p class="elementor-heading-title elementor-size-default">Hitung Mundur Acara
																	</p>		</div>
															</div>
															<div class="elementor-element elementor-element-64cde14 wdp-sticky-section-no elementor-widget elementor-widget-eael-countdown" data-id="64cde14" data-element_type="widget" data-aos="zoom-in" data-aos-duration="1000" data-widget_type="eael-countdown.default">
																<div class="elementor-widget-container">

																	<div class="eael-countdown-wrapper" data-countdown-id="64cde14" data-expire-type="none" data-countdown-type="due_date">
																		<div class="eael-countdown-container eael-countdown-label-block  eael-countdown-label-block-mobile ">
																			<ul id="eael-countdown-64cde14" class="eael-countdown-items" data-date="Feb 08 2022 14:00:00 +0">
																				<li class="eael-countdown-item"><div class="eael-countdown-days"><span data-days class="eael-countdown-digits" id="hari">00</span><span class="eael-countdown-label" style="margin-top: 10px">Hari</span></div></li>
																				<li class="eael-countdown-item"><div class="eael-countdown-hours"><span data-hours class="eael-countdown-digits" id="jam">00</span><span class="eael-countdown-label" style="margin-top: 10px">Jam</span></div></li>
																				<li class="eael-countdown-item"><div class="eael-countdown-minutes"><span data-minutes class="eael-countdown-digits" id="menit">00</span><span class="eael-countdown-label" style="margin-top: 10px">Menit</span></div></li>
																				<li class="eael-countdown-item"><div class="eael-countdown-seconds"><span data-seconds class="eael-countdown-digits" id="detik">00</span><span class="eael-countdown-label" style="margin-top: 10px">Detik</span></div></li>				</ul>
																			<div class="eael-countdown-expiry-template" style="display: none;">
																			</div>
																			<div class="clearfix"></div>
																		</div>
																	</div>

																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</section>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section class="elementor-section elementor-top-section elementor-element elementor-element-7a018455 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="7a018455" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;gradient&quot;,&quot;shape_divider_bottom&quot;:&quot;opacity-tilt&quot;}">
				<div class="elementor-shape elementor-shape-bottom" data-negative="false">
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 2600 131.1" preserveAspectRatio="none">
						<path class="elementor-shape-fill" d="M0 0L2600 0 2600 69.1 0 0z"/>
						<path class="elementor-shape-fill" style="opacity:0.5" d="M0 0L2600 0 2600 69.1 0 69.1z"/>
						<path class="elementor-shape-fill" style="opacity:0.25" d="M2600 0L0 0 0 130.1 2600 69.1z"/>
					</svg>		</div>
				<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-row">
						<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-414f6778 wdp-sticky-section-no" data-id="414f6778" data-element_type="column">
							<div class="elementor-column-wrap elementor-element-populated">
								<div class="elementor-widget-wrap">
									<div class="elementor-element elementor-element-16b3f411 wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="16b3f411" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;none&quot;}" data-aos="zoom-in" data-aos-duration="1000" data-widget_type="heading.default">
										<div class="elementor-widget-container">
											<p class="elementor-heading-title elementor-size-default">Merupakan sebuah kehormatan dan kebahagiaan bagi kami apabila Bapak/Ibu/Saudara/i berkenan</br> hadir untuk memberikan doa restu.</br>Atas kehadirannya kami ucapkan</br> terima kasih.</p>		</div>
									</div>
									<div class="elementor-element elementor-element-6fc67a9b wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="6fc67a9b" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;none&quot;}" data-aos="zoom-in" data-aos-duration="1000" data-widget_type="heading.default">
										<div class="elementor-widget-container">
											<p class="elementor-heading-title elementor-size-default">Kami Yang Berbahagia <br>
												Keluarga Ni Luh Sita Ardani</p>		</div>
									</div>
									<div class="elementor-element elementor-element-2a3af76a wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="2a3af76a" data-element_type="widget" data-widget_type="spacer.default">
										<div class="elementor-widget-container">
											<div class="elementor-spacer">
												<div class="elementor-spacer-inner"></div>
											</div>
										</div>
									</div>
									<div style="margin-bottom: 100px" class="elementor-element elementor-element-533ae00 wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="533ae00" data-element_type="widget" data-aos="zoom-in" data-aos-duration="1000" data-widget_type="heading.default">
										<div class="elementor-widget-container">
											<p class="elementor-heading-title elementor-size-default">Om Shanti, Shanti, Shanti Om
											</p>		</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>

		</div>
	</div>
</div>

<div data-elementor-type="popup" data-elementor-id="29971" class="elementor elementor-29971 elementor-location-popup" data-elementor-settings="{&quot;exit_animation&quot;:&quot;slideInUp&quot;,&quot;entrance_animation_duration&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:0.8,&quot;sizes&quot;:[]},&quot;prevent_close_on_background_click&quot;:&quot;yes&quot;,&quot;prevent_close_on_esc_key&quot;:&quot;yes&quot;,&quot;prevent_scroll&quot;:&quot;yes&quot;,&quot;avoid_multiple_popups&quot;:&quot;yes&quot;,&quot;exit_animation_mobile&quot;:&quot;slideInUp&quot;,&quot;triggers&quot;:{&quot;page_load&quot;:&quot;yes&quot;,&quot;page_load_delay&quot;:0},&quot;timing&quot;:[]}" >
	<div class="elementor-section-wrap">
		<section class="elementor-section elementor-top-section elementor-element elementor-element-23e7237 elementor-section-height-min-height elementor-section-items-bottom elementor-section-boxed elementor-section-height-default wdp-sticky-section-no" data-id="23e7237" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
			<div class="elementor-container elementor-column-gap-default">
				<div class="elementor-row">
					<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-1a5d9854 wdp-sticky-section-no" data-id="1a5d9854" data-element_type="column">
						<div class="elementor-column-wrap elementor-element-populated">
							<div class="elementor-widget-wrap">
								<div class="elementor-element elementor-element-1b112b8 wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="1b112b8" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
									<div class="elementor-widget-container">
										<p class="elementor-heading-title elementor-size-default" style="color: #d99a14">Upacara<br>Tiga Bulan</p>		</div>
								</div>
								<div class="elementor-element elementor-element-f5ba52a wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="f5ba52a" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;none&quot;}" data-widget_type="heading.default">
									<div class="elementor-widget-container">
										<p class="elementor-heading-title elementor-size-default" style="color: #d99a14">Aska</p>		</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="elementor-section elementor-top-section elementor-element elementor-element-7a4fc49 elementor-section-height-min-height elementor-section-items-stretch elementor-section-boxed elementor-section-height-default wdp-sticky-section-no" data-id="7a4fc49" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
			<div class="elementor-background-overlay"></div>
			<div class="elementor-container elementor-column-gap-default">
				<div class="elementor-row">
					<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-6bfe254c wdp-sticky-section-no" data-id="6bfe254c" data-element_type="column">
						<div class="elementor-column-wrap elementor-element-populated">
							<div class="elementor-widget-wrap">
								<div class="elementor-element elementor-element-6380d913 wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="6380d913" data-element_type="widget" data-widget_type="heading.default">
									<div class="elementor-widget-container">
										<p class="elementor-heading-title elementor-size-default">yth. Kepada Bpk/Ibu/saudara/i


										</p>		</div>
								</div>
								<div class="elementor-element elementor-element-52c86ffe close-popup wdp-sticky-section-no elementor-widget elementor-widget-html" data-id="52c86ffe" data-element_type="widget" data-widget_type="html.default">
									<div class="elementor-widget-container">
										<style>
											.button-undangan {
												border: none;
												color: white;
												padding: 10px 32px;
												text-align: center;
												text-decoration: none;
												display: inline-block;
												font-size: 16px;
												margin: 5px 10px;
												transition-duration: 0.4s;
												cursor: pointer;
											}

											.button-undangan {
												background-color: #D0AA84;
												color:black;
												border: 2px solid #fff;
												border-radius: 50px
											}

											.button-undangan:hover {
												background-color: #AF6E06;
												color: white;
												border: 1px solid #FFff;
											}

										</style>

										<center>
											<button
												onclick="playAudio()"
												class="button-undangan" ><i class="fa fa-envelope-open"></i>  Buka Undangan
											</button>


										</center>
										<script>
											var x = document.getElementById("song");
											var y = document.elementorProFrontend.modules.popup.closePopup("{}, event ");

											function playAudio() {
												x.play();
											}

											function pauseAudio() {
												x.pause();
											}
										</script>

										<script>

											jQuery( document ).ready( function( $ ) {
												$( document ).on( 'click', '.close-popup', function( event ) {
													elementorProFrontend.modules.popup.closePopup( {}, event );
												} );
											} );


										</script>		</div>
								</div>
								<div class="elementor-element elementor-element-5ad3006 wdp-sticky-section-no elementor-widget elementor-widget-html" data-id="5ad3006" data-element_type="widget" data-widget_type="html.default">
									<div class="elementor-widget-container">
										<script>

											jQuery( document ).ready( function( $ ) {
												$( document ).on( 'click', '.close-popup', function( event ) {
													elementorProFrontend.modules.popup.closePopup( {}, event );
												} );
											} );


										</script>		</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
</div><script>
	// Set the date we're counting down to
	var countDownDate = new Date("Oct 27, 2022 10:00:00").getTime();

	// Update the count down every 1 second
	var x = setInterval(function() {

		// Get today's date and time
		var now = new Date().getTime();

		// Find the distance between now and the count down date
		var distance = countDownDate - now;

		// Time calculations for days, hours, minutes and seconds
		var days = Math.floor(distance / (1000 * 60 * 60 * 24));
		var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
		var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
		var seconds = Math.floor((distance % (1000 * 60)) / 1000);

		// Output the result in an element with id="demo"
		document.getElementById("hari").innerHTML = days;
		document.getElementById("jam").innerHTML = hours;
		document.getElementById("menit").innerHTML = minutes;
		document.getElementById("detik").innerHTML = seconds;

		// If the count down is over, write some text
		if (distance < 0) {
			clearInterval(x);
			document.getElementById("demo").innerHTML = "EXPIRED";
		}
	}, 1000);
</script>
<link rel='stylesheet' id='elementor-gallery-css'  href='<?=base_url()?>assets/wp-content/plugins/elementor/assets/lib/e-gallery/css/e-gallery.min7359.css?ver=1.2.0' media='all' />
<link rel='stylesheet' id='e-animations-css'  href='<?=base_url()?>assets/wp-content/plugins/elementor/assets/lib/animations/animations.mina4f3.css?ver=3.6.8' media='all' />
<script src='<?=base_url()?>assets/wp-content/plugins/click-to-chat-for-whatsapp/new/inc/assets/js/app16e3.js?ver=3.14' id='ht_ctc_app_js-js'></script>
<script id='wdp_js_script-js-extra'>
	var WDP_WP = {"ajaxurl":"https:\/\/undangonlinebali.com\/wp-admin\/admin-ajax.php","wdpNonce":"2f2c2ad0ec","jpages":"true","jPagesNum":"5","textCounter":"true","textCounterNum":"500","widthWrap":"","autoLoad":"true","thanksComment":"Terima kasih atas ucapan & doanya!","thanksReplyComment":"Terima kasih atas balasannya!","duplicateComment":"You might have left one of the fields blank, or duplicate comments","insertImage":"Insert image","insertVideo":"Insert video","insertLink":"Insert link","checkVideo":"Check video","accept":"Accept","cancel":"Cancel","reply":"Balas","textWriteComment":"Tulis Ucapan & Doa","classPopularComment":"wdp-popular-comment","textUrlImage":"Url image","textUrlVideo":"Url video youtube or vimeo","textUrlLink":"Url link","textToDisplay":"Text to display","textCharacteresMin":"Minimal 2 karakter","textNavNext":"Selanjutnya","textNavPrev":"Sebelumnya","textMsgDeleteComment":"Do you want delete this comment?","textLoadMore":"Load more","textLikes":"Likes"};
</script>
<script src='<?=base_url()?>assets/wp-content/plugins/weddingpress/addons/comment-kit/js/wdp_scriptd537.js?ver=2.7.6' id='wdp_js_script-js'></script>
<script src='<?=base_url()?>assets/wp-content/plugins/weddingpress/addons/comment-kit/js/libs/jquery.jPages.minac14.js?ver=0.7' id='wdp_jPages-js'></script>
<script src='<?=base_url()?>assets/wp-content/plugins/weddingpress/addons/comment-kit/js/libs/jquery.textareaCounterd5f7.js?ver=2.0' id='wdp_textCounter-js'></script>
<script src='<?=base_url()?>assets/wp-content/plugins/weddingpress/addons/comment-kit/js/libs/jquery.placeholder.min5d0a.js?ver=2.0.7' id='wdp_placeholder-js'></script>
<script src='<?=base_url()?>assets/wp-content/plugins/weddingpress/addons/comment-kit/js/libs/autosize.mind50e.js?ver=1.14' id='wdp_autosize-js'></script>
<script src='<?=base_url()?>assets/wp-content/plugins/weddingpress/assets/js/wdp-swiper.min.js' id='wdp-swiper-js-js'></script>
<script src='<?=base_url()?>assets/wp-content/plugins/weddingpress/assets/js/qr-code.js' id='weddingpress-qr-js'></script>
<script src='<?=base_url()?>assets/wp-content/plugins/weddingpress/assets/js/wdp-horizontal.js' id='wdp-horizontal-js-js'></script>
<script src='<?=base_url()?>assets/wp-content/plugins/weddingpress/assets/js/exad-scripts.min292d.js?ver=2.8.8' id='exad-main-script-js'></script>
<script id='elementor-frontend-js-before'>
	var elementorFrontendConfig = {"environmentMode":{"edit":false,"wpPreview":false,"isScriptDebug":false},"i18n":{"shareOnFacebook":"Bagikan di Facebook","shareOnTwitter":"Bagikan di Twitter","pinIt":"Buat Pin","download":"Unduh","downloadImage":"Unduh gambar","fullscreen":"Layar Penuh","zoom":"Perbesar","share":"Bagikan","playVideo":"Putar Video","previous":"Sebelumnya","next":"Selanjutnya","close":"Tutup"},"is_rtl":false,"breakpoints":{"xs":0,"sm":480,"md":768,"lg":1025,"xl":1440,"xxl":1600},"responsive":{"breakpoints":{"mobile":{"label":"Ponsel","value":767,"default_value":767,"direction":"max","is_enabled":true},"mobile_extra":{"label":"Ekstra Seluler","value":880,"default_value":880,"direction":"max","is_enabled":false},"tablet":{"label":"Tablet","value":1024,"default_value":1024,"direction":"max","is_enabled":true},"tablet_extra":{"label":"Tablet Ekstra","value":1200,"default_value":1200,"direction":"max","is_enabled":false},"laptop":{"label":"Laptop","value":1366,"default_value":1366,"direction":"max","is_enabled":false},"widescreen":{"label":"Layar lebar","value":2400,"default_value":2400,"direction":"min","is_enabled":false}}},"version":"3.6.8","is_static":false,"experimentalFeatures":{"e_import_export":true,"e_hidden_wordpress_widgets":true,"theme_builder_v2":true,"landing-pages":true,"elements-color-picker":true,"favorite-widgets":true,"admin-top-bar":true,"page-transitions":true,"notes":true,"form-submissions":true,"e_scroll_snap":true},"urls":{"assets":"https:\/\/undangonlinebali.com\/wp-content\/plugins\/elementor\/assets\/"},"settings":{"page":[],"editorPreferences":[]},"kit":{"active_breakpoints":["viewport_mobile","viewport_tablet"],"global_image_lightbox":"yes","lightbox_enable_counter":"yes","lightbox_enable_fullscreen":"yes","lightbox_enable_zoom":"yes","lightbox_enable_share":"yes","lightbox_title_src":"title","lightbox_description_src":"description"},"post":{"id":34320,"title":"Tiga%20Otonan%20Dinda%20-%20Undangan%20Online%20Bali","excerpt":"Kamis, 27 oktober 2022","featuredImage":"https:\/\/undangonlinebali.com\/wp-content\/uploads\/2022\/01\/Tiga-Otonan-Dinda-4-768x1024.jpg"}};
</script>
<script id='rocket-browser-checker-js-after'>
	"use strict";var _createClass=function(){function defineProperties(target,props){for(var i=0;i<props.length;i++){var descriptor=props[i];descriptor.enumerable=descriptor.enumerable||!1,descriptor.configurable=!0,"value"in descriptor&&(descriptor.writable=!0),Object.defineProperty(target,descriptor.key,descriptor)}}return function(Constructor,protoProps,staticProps){return protoProps&&defineProperties(Constructor.prototype,protoProps),staticProps&&defineProperties(Constructor,staticProps),Constructor}}();function _classCallCheck(instance,Constructor){if(!(instance instanceof Constructor))throw new TypeError("Cannot call a class as a function")}var RocketBrowserCompatibilityChecker=function(){function RocketBrowserCompatibilityChecker(options){_classCallCheck(this,RocketBrowserCompatibilityChecker),this.passiveSupported=!1,this._checkPassiveOption(this),this.options=!!this.passiveSupported&&options}return _createClass(RocketBrowserCompatibilityChecker,[{key:"_checkPassiveOption",value:function(self){try{var options={get passive(){return!(self.passiveSupported=!0)}};window.addEventListener("test",null,options),window.removeEventListener("test",null,options)}catch(err){self.passiveSupported=!1}}},{key:"initRequestIdleCallback",value:function(){!1 in window&&(window.requestIdleCallback=function(cb){var start=Date.now();return setTimeout(function(){cb({didTimeout:!1,timeRemaining:function(){return Math.max(0,50-(Date.now()-start))}})},1)}),!1 in window&&(window.cancelIdleCallback=function(id){return clearTimeout(id)})}},{key:"isDataSaverModeOn",value:function(){return"connection"in navigator&&!0===navigator.connection.saveData}},{key:"supportsLinkPrefetch",value:function(){var elem=document.createElement("link");return elem.relList&&elem.relList.supports&&elem.relList.supports("prefetch")&&window.IntersectionObserver&&"isIntersecting"in IntersectionObserverEntry.prototype}},{key:"isSlowConnection",value:function(){return"connection"in navigator&&"effectiveType"in navigator.connection&&("2g"===navigator.connection.effectiveType||"slow-2g"===navigator.connection.effectiveType)}}]),RocketBrowserCompatibilityChecker}();
</script>
<script id='rocket-preload-links-js-extra'>
	var RocketPreloadLinksConfig = {"excludeUris":"\/(?:.+\/)?feed(?:\/(?:.+\/?)?)?$|\/(?:.+\/)?embed\/|\/(index\\.php\/)?wp\\-json(\/.*|$)|\/refer\/|\/go\/|\/recommend\/|\/recommends\/","usesTrailingSlash":"1","imageExt":"jpg|jpeg|gif|png|tiff|bmp|webp|avif|pdf|doc|docx|xls|xlsx|php","fileExt":"jpg|jpeg|gif|png|tiff|bmp|webp|avif|pdf|doc|docx|xls|xlsx|php|html|htm","siteUrl":"https:\/\/undangonlinebali.com","onHoverDelay":"100","rateThrottle":"3"};
</script>
<script id='rocket-preload-links-js-after'>
	(function() {
		"use strict";var r="function"==typeof Symbol&&"symbol"==typeof Symbol.iterator?function(e){return typeof e}:function(e){return e&&"function"==typeof Symbol&&e.constructor===Symbol&&e!==Symbol.prototype?"symbol":typeof e},e=function(){function i(e,t){for(var n=0;n<t.length;n++){var i=t[n];i.enumerable=i.enumerable||!1,i.configurable=!0,"value"in i&&(i.writable=!0),Object.defineProperty(e,i.key,i)}}return function(e,t,n){return t&&i(e.prototype,t),n&&i(e,n),e}}();function i(e,t){if(!(e instanceof t))throw new TypeError("Cannot call a class as a function")}var t=function(){function n(e,t){i(this,n),this.browser=e,this.config=t,this.options=this.browser.options,this.prefetched=new Set,this.eventTime=null,this.threshold=1111,this.numOnHover=0}return e(n,[{key:"init",value:function(){!this.browser.supportsLinkPrefetch()||this.browser.isDataSaverModeOn()||this.browser.isSlowConnection()||(this.regex={excludeUris:RegExp(this.config.excludeUris,"i"),images:RegExp(".("+this.config.imageExt+")$","i"),fileExt:RegExp(".("+this.config.fileExt+")$","i")},this._initListeners(this))}},{key:"_initListeners",value:function(e){-1<this.config.onHoverDelay&&document.addEventListener("mouseover",e.listener.bind(e),e.listenerOptions),document.addEventListener("mousedown",e.listener.bind(e),e.listenerOptions),document.addEventListener("touchstart",e.listener.bind(e),e.listenerOptions)}},{key:"listener",value:function(e){var t=e.target.closest("a"),n=this._prepareUrl(t);if(null!==n)switch(e.type){case"mousedown":case"touchstart":this._addPrefetchLink(n);break;case"mouseover":this._earlyPrefetch(t,n,"mouseout")}}},{key:"_earlyPrefetch",value:function(t,e,n){var i=this,r=setTimeout(function(){if(r=null,0===i.numOnHover)setTimeout(function(){return i.numOnHover=0},1e3);else if(i.numOnHover>i.config.rateThrottle)return;i.numOnHover++,i._addPrefetchLink(e)},this.config.onHoverDelay);t.addEventListener(n,function e(){t.removeEventListener(n,e,{passive:!0}),null!==r&&(clearTimeout(r),r=null)},{passive:!0})}},{key:"_addPrefetchLink",value:function(i){return this.prefetched.add(i.href),new Promise(function(e,t){var n=document.createElement("link");n.rel="prefetch",n.href=i.href,n.onload=e,n.onerror=t,document.head.appendChild(n)}).catch(function(){})}},{key:"_prepareUrl",value:function(e){if(null===e||"object"!==(void 0===e?"undefined":r(e))||!1 in e||-1===["http:","https:"].indexOf(e.protocol))return null;var t=e.href.substring(0,this.config.siteUrl.length),n=this._getPathname(e.href,t),i={original:e.href,protocol:e.protocol,origin:t,pathname:n,href:t+n};return this._isLinkOk(i)?i:null}},{key:"_getPathname",value:function(e,t){var n=t?e.substring(this.config.siteUrl.length):e;return n.startsWith("https://undangonlinebali.com/")||(n="/"+n),this._shouldAddTrailingSlash(n)?n+"/":n}},{key:"_shouldAddTrailingSlash",value:function(e){return this.config.usesTrailingSlash&&!e.endsWith("https://undangonlinebali.com/")&&!this.regex.fileExt.test(e)}},{key:"_isLinkOk",value:function(e){return null!==e&&"object"===(void 0===e?"undefined":r(e))&&(!this.prefetched.has(e.href)&&e.origin===this.config.siteUrl&&-1===e.href.indexOf("?")&&-1===e.href.indexOf("#")&&!this.regex.excludeUris.test(e.href)&&!this.regex.images.test(e.href))}}],[{key:"run",value:function(){"undefined"!=typeof RocketPreloadLinksConfig&&new n(new RocketBrowserCompatibilityChecker({capture:!0,passive:!0}),RocketPreloadLinksConfig).init()}}]),n}();t.run();
	}());
</script>
<script src='<?=base_url()?>assets/wp-content/plugins/essential-addons-for-elementor-lite/assets/front-end/js/view/general.min9dff.js?ver=5.3.2' id='eael-general-js'></script>
<script src='<?=base_url()?>assets/wp-content/uploads/essential-addons-elementor/eael-343204455.js?ver=1643628406' id='eael-34320-js'></script>
<script src='<?=base_url()?>assets/wp-content/plugins/elementskit-lite/libs/framework/assets/js/frontend-scriptde39.js?ver=2.7.3' id='elementskit-framework-js-frontend-js'></script>
<script id='elementskit-framework-js-frontend-js-after'>
	var elementskit = {
		resturl: 'https://undangonlinebali.com/wp-json/elementskit/v1/',
	}


</script>
<script src='<?=base_url()?>assets/wp-content/plugins/elementskit-lite/widgets/init/assets/js/widget-scriptsde39.js?ver=2.7.3' id='ekit-widget-scripts-js'></script>
<script src='<?=base_url()?>assets/wp-content/plugins/elementor/assets/lib/e-gallery/js/e-gallery.min7359.js?ver=1.2.0' id='elementor-gallery-js'></script>
<script src='<?=base_url()?>assets/wp-content/plugins/elementor-pro/assets/js/webpack-pro.runtime.min1ac1.js?ver=3.7.3' id='elementor-pro-webpack-runtime-js'></script>
<script src='<?=base_url()?>assets/wp-content/plugins/elementor/assets/js/webpack.runtime.mina4f3.js?ver=3.6.8' id='elementor-webpack-runtime-js'></script>
<script src='<?=base_url()?>assets/wp-content/plugins/elementor/assets/js/frontend-modules.mina4f3.js?ver=3.6.8' id='elementor-frontend-modules-js'></script>
<script src='<?=base_url()?>assets/wp-includes/js/dist/vendor/regenerator-runtime.min3937.js?ver=0.13.9' id='regenerator-runtime-js'></script>
<script src='<?=base_url()?>assets/wp-includes/js/dist/vendor/wp-polyfill.min2c7c.js?ver=3.15.0' id='wp-polyfill-js'></script>
<script src='<?=base_url()?>assets/wp-includes/js/dist/hooks.min8cbb.js?ver=1e58c8c5a32b2e97491080c5b10dc71c' id='wp-hooks-js'></script>
<script src='<?=base_url()?>assets/wp-includes/js/dist/i18n.mina28b.js?ver=30fcecb428a0e8383d3776bcdd3a7834' id='wp-i18n-js'></script>
<script id='wp-i18n-js-after'>
	wp.i18n.setLocaleData( { 'text direction\u0004ltr': [ 'ltr' ] } );
</script>
<script id='elementor-pro-frontend-js-translations'>
	( function( domain, translations ) {
		var localeData = translations.locale_data[ domain ] || translations.locale_data.messages;
		localeData[""].domain = domain;
		wp.i18n.setLocaleData( localeData, domain );
	} )( "elementor-pro", { "locale_data": { "messages": { "": {} } } } );
</script>
<script id='elementor-pro-frontend-js-before'>
	var ElementorProFrontendConfig = {"ajaxurl":"https:\/\/undangonlinebali.com\/wp-admin\/admin-ajax.php","nonce":"b20dbc43fc","urls":{"assets":"https:\/\/undangonlinebali.com\/wp-content\/plugins\/elementor-pro\/assets\/","rest":"https:\/\/undangonlinebali.com\/wp-json\/"},"shareButtonsNetworks":{"facebook":{"title":"Facebook","has_counter":true},"twitter":{"title":"Twitter"},"linkedin":{"title":"LinkedIn","has_counter":true},"pinterest":{"title":"Pinterest","has_counter":true},"reddit":{"title":"Reddit","has_counter":true},"vk":{"title":"VK","has_counter":true},"odnoklassniki":{"title":"OK","has_counter":true},"tumblr":{"title":"Tumblr"},"digg":{"title":"Digg"},"skype":{"title":"Skype"},"stumbleupon":{"title":"StumbleUpon","has_counter":true},"mix":{"title":"Mix"},"telegram":{"title":"Telegram"},"pocket":{"title":"Pocket","has_counter":true},"xing":{"title":"XING","has_counter":true},"whatsapp":{"title":"WhatsApp"},"email":{"title":"Email"},"print":{"title":"Print"}},"facebook_sdk":{"lang":"id_ID","app_id":""},"lottie":{"defaultAnimationUrl":"https:\/\/undangonlinebali.com\/wp-content\/plugins\/elementor-pro\/modules\/lottie\/assets\/animations\/default.json"}};
</script>
<script src='<?=base_url()?>assets/wp-content/plugins/elementor-pro/assets/js/frontend.min1ac1.js?ver=3.7.3' id='elementor-pro-frontend-js'></script>
<script src='<?=base_url()?>assets/wp-content/plugins/elementor/assets/lib/waypoints/waypoints.min05da.js?ver=4.0.2' id='elementor-waypoints-js'></script>
<script src='<?=base_url()?>assets/wp-includes/js/jquery/ui/core.min0028.js?ver=1.13.1' id='jquery-ui-core-js'></script>
<script src='<?=base_url()?>assets/wp-content/plugins/elementor/assets/lib/swiper/swiper.min48f5.js?ver=5.3.6' id='swiper-js'></script>
<script src='<?=base_url()?>assets/wp-content/plugins/elementor/assets/lib/share-link/share-link.mina4f3.js?ver=3.6.8' id='share-link-js'></script>
<script src='<?=base_url()?>assets/wp-content/plugins/elementor/assets/lib/dialog/dialog.mind227.js?ver=4.9.0' id='elementor-dialog-js'></script>

<script src='<?=base_url()?>assets/wp-content/plugins/elementor/assets/js/frontend.mina4f3.js?ver=3.6.8' id='elementor-frontend-js'></script>
<script src='<?=base_url()?>assets/wp-content/plugins/elementor-pro/assets/js/preloaded-elements-handlers.min1ac1.js?ver=3.7.3' id='pro-preloaded-elements-handlers-js'></script>
<script src='<?=base_url()?>assets/wp-content/plugins/elementskit-lite/widgets/init/assets/js/animate-circlede39.js?ver=2.7.3' id='animate-circle-js'></script>
<script id='elementskit-elementor-js-extra'>
	var ekit_config = {"ajaxurl":"https:\/\/undangonlinebali.com\/wp-admin\/admin-ajax.php","nonce":"e5c0192b65"};
</script>
<script src='<?=base_url()?>assets/wp-content/plugins/elementskit-lite/widgets/init/assets/js/elementorde39.js?ver=2.7.3' id='elementskit-elementor-js'></script>
<script src='<?=base_url()?>assets/wp-content/plugins/elementor/assets/js/preloaded-modules.mina4f3.js?ver=3.6.8' id='preloaded-modules-js'></script>
<script src='<?=base_url()?>assets/wp-content/plugins/elementor-pro/assets/lib/sticky/jquery.sticky.min1ac1.js?ver=3.7.3' id='e-sticky-js'></script>
<script id='weddingpress-wdp-js-extra'>
	var cevar = {"ajax_url":"https:\/\/undangonlinebali.com\/wp-admin\/admin-ajax.php","plugin_url":"https:\/\/undangonlinebali.com\/wp-content\/plugins\/weddingpress\/"};
</script>
<script src='<?=base_url()?>assets/wp-content/plugins/weddingpress/assets/js/wdp.min292d.js?ver=2.8.8' id='weddingpress-wdp-js'></script>
<script id='powerpack-frontend-js-extra'>
	var ppLogin = {"empty_username":"Enter a username or email address.","empty_password":"Enter password.","empty_password_1":"Enter a password.","empty_password_2":"Re-enter password.","empty_recaptcha":"Please check the captcha to verify you are not a robot.","email_sent":"A password reset email has been sent to the email address for your account, but may take several minutes to show up in your inbox. Please wait at least 10 minutes before attempting another reset.","reset_success":"Your password has been reset successfully.","ajax_url":"https:\/\/undangonlinebali.com\/wp-admin\/admin-ajax.php"};
	var ppRegistration = {"invalid_username":"This username is invalid because it uses illegal characters. Please enter a valid username.","username_exists":"This username is already registered. Please choose another one.","empty_email":"Please type your email address.","invalid_email":"The email address isn\u2019t correct!","email_exists":"The email is already registered, please choose another one.","password":"Password must not contain the character \"\\\\\"","password_length":"Your password should be at least 8 characters long.","password_mismatch":"Password does not match.","invalid_url":"URL seems to be invalid.","recaptcha_php_ver":"reCAPTCHA API requires PHP version 5.3 or above.","recaptcha_missing_key":"Your reCAPTCHA Site or Secret Key is missing!","show_password":"Show password","hide_password":"Hide password","ajax_url":"https:\/\/undangonlinebali.com\/wp-admin\/admin-ajax.php"};
	var ppCoupons = {"copied_text":"Copied"};
</script>
<script src='<?=base_url()?>assets/wp-content/plugins/powerpack-elements/assets/js/min/frontend.min562c.js?ver=2.9.11' id='powerpack-frontend-js'></script>
<script src='<?=base_url()?>assets/wp-content/plugins/powerpack-elements/assets/lib/tooltipster/tooltipster.min562c.js?ver=2.9.11' id='pp-tooltipster-js'></script>
<script src="<?=base_url()?>assets/unpkg.com/aos%403.0.0-beta.6/dist/aos.js"></script>
<script>
	AOS.init();
</script>

<div class="pafe-break-point" data-pafe-break-point-md="768" data-pafe-break-point-lg="1025" data-pafe-ajax-url="https://undangonlinebali.com/wp-admin/admin-ajax.php"></div><div data-pafe-form-builder-tinymce-upload="https://undangonlinebali.com/wp-content/plugins/piotnet-addons-for-elementor-pro/inc/tinymce/tinymce-upload.php"></div><div data-pafe-plugin-url="https://undangonlinebali.com/wp-content/plugins"></div>	</body>

</html>
